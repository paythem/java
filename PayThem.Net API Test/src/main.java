import java.io.IOException;

public class main {
    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("PayThem.Net API integration");
        System.out.println("");
        System.out.println("Requirements:");
        System.out.println("- Include net.paythem.jar in CLASSPATH");
        System.out.println("- Credentials and environment/domain must have been supplied by PayThem.net");
        System.out.println("");
        System.out.println("Implementation:");
        System.out.println("- Replace [VALUE] with your own / supplied values.");
        System.out.println("");
        System.out.println("import net.paythem.api.PTN_API_v2;");
        System.out.println("PTN_API_v2 api = new PTN_API_v2(");
        System.out.println("	\"[ENVIRONMENT]\",");
        System.out.println("	\"[PUBLIC_KEY]\",");
        System.out.println("	\"[PRIVATE_KEY]\",");
        System.out.println("	\"[USERNAME]\",");
        System.out.println("	\"[PASSWORD]\",");
        System.out.println("	\"[UNIQUE_RANDOM_IV_OF_16_CHARACTERS]\",");
        System.out.println("	\"[YOUR_SERVER_ID]\",");
        System.out.println("	\"[CURRENT_TIMEZONE]\",");
        System.out.println(");");
        System.out.println("");
        System.out.println("");
        System.out.println("Available functions:");
        System.out.println("- get_OEMList");
        System.out.println("- get_BrandList");
        System.out.println("- get_ProductList");
        System.out.println("- get_ProductAvailability");
        System.out.println("- get_ProductOutputFormats");
        System.out.println("- get_Vouchers");
        System.out.println("- get_SalesTransaction_ByDateRange");
        System.out.println("- get_FinancialTransaction_ByDateRange");
        System.out.println("");
        System.out.println("All responses are JSON strings. They must be converted to JSON objects for usage.");
        System.out.println("Follow the API documentation and implement calls accordingly.");
        System.out.println("");
        System.out.println("");
        System.out.println("Available utility functions:");
        System.out.println("- setConnectTimeout");
        System.out.println("- setReadTimeout");
        System.out.println("- getCurrentTimeStamp");
        System.out.println("");
        System.out.println("");
    }
}