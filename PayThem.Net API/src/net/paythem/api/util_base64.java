package net.paythem.api;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

class util_base64 {
    /*
        FROM https://stackoverflow.com/questions/24429734/why-phps-hash-hmacsha256-gives-different-result-than-java-sha256-hmac
     */
    public static String base64sha256(String data, String secret) {
        String hash;
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] res = sha256_HMAC.doFinal(data.getBytes(StandardCharsets.UTF_8));
            hash = getHex(res);
            return hash;
        } catch (Exception ignored){}
        return "";
    }

    /*
        FROM https://stackoverflow.com/questions/24429734/why-phps-hash-hmacsha256-gives-different-result-than-java-sha256-hmac
     */
    static final String HEXES = "0123456789abcdef";
    public static String getHex( byte [] raw ) {
        if ( raw == null ) {
            return null;
        }
        final StringBuilder hex = new StringBuilder( 2 * raw.length );
        for ( final byte b : raw ) {
            hex.append(HEXES.charAt((b & 0xF0) >> 4))
                    .append(HEXES.charAt((b & 0x0F)));
        }
        return hex.toString();
    }

}
