/**
 * Public class for accessing the PayThem.Net public voucher purchase API on the VVS platform.
 * 
 * This class exposes all available calls to the 2824 API ID on the PayThem system.
 * 
 * <a href="https://paythem.net">PayThem.Net website</a>.
 * 
 * JavaDoc to be used in conjunction with Web API documentation. The Web API documentation will explain the return string for each call.
 * 
 * @copyright PayThem.Net LLC [Australia] 2020
 * @author Richard S. de Breyn
 */
package net.paythem.api;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class PTN_API_v2 {
	apiCaller caller;

	/**
	 * <p>Constructor for PayThem API, protocol version 2.0.4</p>
	 * 
	 * The following parameters are supplied by PayThem.Net during take-on:<ul>
	 * <li>_domain</li>
	 * <li>_publicKey</li>
	 * <li>_privateKey</li>
	 * <li>_username</li>
	 * <li>_password</li>
	 * <li></li>
	 * <li></li>
	 * </ul>
	 * 
	 *  @param String _domain The environment that the call will be placed to.
	 *  @param String _publicKey The public key that will be used to identify the user doing the calls.
	 *  @param String _privateKey The private key used to encrypt the communications.
	 *  @param String _username The username to authorize.
	 *  @param String _password The password to authenticate.
	 *  @param String _IV The IV to use in the encryption. Passed to the server. Please ensure that the IV is always a unique, 16 alphanumeric characters to ensure security.
	 *  @param String _sourceIP To represent and correspond to the server public IP.
	 *  @param String _timeZone The local server's time zone. Will be used to determine time.
	 */
	public PTN_API_v2(String _domain, String _publicKey, String _privateKey, String _username, String _password,
			String _IV, String _sourceIP, String _timeZone) {
		caller = new apiCaller(_domain, _publicKey, _privateKey, _username, _password, _IV, _sourceIP, _timeZone);
	}

	/**
	 * Set the maximum time the API will attempt to connect to VVS API.
	 * @param int _newVal The new timeout value.
	 */
	public void setConnectTimeout(int _newVal) {
		caller.setConnectTimeout(_newVal);
	}

	/**
	 * The maximum time the API will wait for response from VVS before the call will fail.
	 * @param int _newVal The new timeout value.
	 */
	public void setReadTimeout(int _newVal) {
		caller.setReadTimeout(_newVal);
	}

	/**
	 * Generate and return the current date / time. 
	 * @return DateTimeFormatter a date / time in the format of yyyy/mm/dd HH:mm:ss.
	 */
	public String getCurrentTimeStamp() {
		return DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").format(LocalDateTime.now());
	}

	/**
	 * Get the OEMs available on this profile.
	 * @param String _timeStamp for overriding the current time with your own time.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_OEMList(String _timeStamp) throws IOException {
		return caller.callAPI("get_OEMList", _timeStamp);
	}

	/**
	 * Get the OEMs available on this profile.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_OEMList() throws IOException {
		return get_OEMList(getCurrentTimeStamp());
	}

	/**
	 * Get the Brands available on this profile.
	 * @param String _timeStamp for overriding the current time with your own time.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_BrandList(String _timeStamp) throws IOException {
		return caller.callAPI("get_BrandList", _timeStamp);
	}

	/**
	 * Get the Brands available on this profile.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_BrandList() throws IOException {
		return get_BrandList(getCurrentTimeStamp());
	}

	/**
	 * Get the Products available on this profile. 
	 * @param String _timeStamp for overriding the current time with your own time.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_ProductList(String _timeStamp) throws IOException {
		return caller.callAPI("get_ProductList", _timeStamp);
	}

	/**
	 * Get the Products available on this profile. 
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_ProductList() throws IOException {
		return get_ProductList(getCurrentTimeStamp());
	}

	/**
	 * Get the product availability for specified product.
	 * @param String _productID The PayThem product ID to purchase voucher.
	 * @param String _timeStamp for overriding the current time with your own time.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_ProductAvailability(String _productID, String _timeStamp) throws IOException {
		HashMap<String, String> paramsApiCall = new HashMap<String, String>();
		paramsApiCall.clear();
		paramsApiCall.put("PRODUCT_ID", _productID);
		return caller.callAPI("get_ProductAvailability", _timeStamp, paramsApiCall);
	}

	/**
	 * Get the product availability for specified product.
	 * @param String _productID The PayThem product ID to purchase voucher.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_ProductAvailability(String _productID) throws IOException {
		return get_ProductAvailability(_productID, getCurrentTimeStamp());
	}

	/**
	 * Retrieve / perform voucher(s) purchase with full parameters specified.
	 * 
	 * Primary call. All other versions of get_Voucher reroutes to this call.
	 * 
	 * @param String _productID The PayThem product ID to purchase voucher.
	 * @param int _quantity The quantity of vouchers to purchase. Depending on account profile, server side defaults to 20.
	 * @param String _timeStamp for overriding the current time with your own time.
	 * @param String _ref The Purchase Reference ID from the source system.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_Vouchers(String _productID, int _quantity, String _timeStamp, String _ref) throws IOException {
		HashMap<String, String> paramsApiCall = new HashMap<String, String>();
		paramsApiCall.clear();
		paramsApiCall.put("PRODUCT_ID", _productID);
		paramsApiCall.put("QUANTITY", Integer.toString(_quantity));
		paramsApiCall.put("REFERENCE_ID", _ref);
		return caller.callAPI("get_Vouchers", _timeStamp, paramsApiCall);
	}

	/**
	 * Retrieve / perform a single voucher of selected product. No other parameters set.
	 * @param String _productID The PayThem product ID to purchase voucher.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_Vouchers(String _productID) throws IOException {
		return get_Vouchers(_productID, 1, getCurrentTimeStamp(), "");
	}

	/**
	 * Retrieve / perform a single voucher of selected product. Source system's Purchase Reference ID is set.
	 * @param String _productID The PayThem product ID to purchase voucher.
	 * @param String _ref The Purchase Reference ID from the source system.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_Vouchers(String _productID, String _ref) throws IOException {
		return get_Vouchers(_productID, 1, getCurrentTimeStamp(), _ref);
	}

	/**
	 * Retrieve specified (_quantity) vouchers, no other parameters set.
	 * @param String _productID The PayThem product ID to purchase voucher.
	 * @param int _quantity The quantity of vouchers to purchase. Depending on account profile, server side defaults to 20.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_Vouchers(String _productID, int _quantity) throws IOException {
		return get_Vouchers(_productID, _quantity, getCurrentTimeStamp(), "");
	}

	/**
	 * Retrieve specified (_quantity) vouchers. Source system's Purchase Reference ID is set.
	 * @param String _productID The PayThem product ID to purchase voucher.
	 * @param int _quantity The quantity of vouchers to purchase. Depending on account profile, server side defaults to 20.
	 * @param String _ref The Purchase Reference ID from the source system.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_Vouchers(String _productID, int _quantity, String _ref) throws IOException {
		return get_Vouchers(_productID, _quantity, getCurrentTimeStamp(), _ref);
	}

	/**
	 * Retrieve single voucher. Source system's Purchase Reference ID and date/time stamp is set.
	 * @param String _productID The PayThem product ID to purchase voucher.
	 * @param String _timeStamp for overriding the current time with your own time.
	 * @param String _ref The Purchase Reference ID from the source system.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_Vouchers(String _productID, String _timeStamp, String _ref) throws IOException {
		return get_Vouchers(_productID, 1, getCurrentTimeStamp(), _ref);
	}

	/**
	 * Get all voucher sales during a specified period. All parameters included.
	 * 
	 * Server limits time difference between _fromDateTime and _toDateTime 30 days.
	 * 
	 * @param String _fromDateTime The starting date of query. YYYY/mm/dd HH:mm:ss format. 
	 * @param String _toDateTime The ending date of the query. YYYY/mm/dd HH:mm:ss format.
	 * @param String _timeStamp for overriding the current time with your own time.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_SalesTransaction_ByDateRange(String _fromDateTime, String _toDateTime, String _timeStamp)
			throws IOException {
		HashMap<String, String> paramsApiCall = new HashMap<String, String>();
		paramsApiCall.clear();
		paramsApiCall.put("FROM_DATE", _fromDateTime);
		paramsApiCall.put("TO_DATE", _toDateTime);
		return caller.callAPI("get_SalesTransaction_ByDateRange", _timeStamp, paramsApiCall);
	}

	/**
	 * Get all voucher sales during a specified period. _timeStamp not specified.
	 * 
	 * Server limits time difference between _fromDateTime and _toDateTime 30 days.
	 * 
	 * @param String _fromDateTime The starting date of query. YYYY/mm/dd HH:mm:ss format. 
	 * @param String _toDateTime The ending date of the query. YYYY/mm/dd HH:mm:ss format.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_SalesTransaction_ByDateRange(String _fromDateTime, String _toDateTime) throws IOException {
		return get_SalesTransaction_ByDateRange(_fromDateTime, _toDateTime, getCurrentTimeStamp());
	}

	/*
	 * 
	 */
	/**
	 * Get all financial transactions (sales, reversals and loads) for a specified period. Includes all parameters.
	 * 
	 * Server limits time difference between _fromDateTime and _toDateTime 30 days.
	 * 
	 * @param String _fromDateTime The starting date of query. YYYY/mm/dd HH:mm:ss format. 
	 * @param String _toDateTime The ending date of the query. YYYY/mm/dd HH:mm:ss format.
	 * @param String _timeStamp for overriding the current time with your own time.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_FinancialTransaction_ByDateRange(String _fromDateTime, String _toDateTime, String _timeStamp)
			throws IOException {
		HashMap<String, String> paramsApiCall = new HashMap<String, String>();
		paramsApiCall.clear();
		paramsApiCall.put("FROM_DATE", _fromDateTime);
		paramsApiCall.put("TO_DATE", _toDateTime);
		return caller.callAPI("get_FinancialTransaction_ByDateRange", _timeStamp, paramsApiCall);
	}

	/**
	 * Get all financial transactions (sales, reversals and loads) for a specified period. _timeStamp not required.
	 * 
	 * Server limits time difference between _fromDateTime and _toDateTime 30 days.
	 * 
	 * @param String _fromDateTime The starting date of query. YYYY/mm/dd HH:mm:ss format. 
	 * @param String _toDateTime The ending date of the query. YYYY/mm/dd HH:mm:ss format.
	 * @return String JSON string representing results. Must be decoded to JSON object for use.
	 * @throws IOException
	 */
	public String get_FinancialTransaction_ByDateRange(String _fromDateTime, String _toDateTime) throws IOException {
		return get_FinancialTransaction_ByDateRange(_fromDateTime, _toDateTime, getCurrentTimeStamp());
	}

	/**
	 * Get availability of all products.
	 * 
	 * @throws IOException
	 */
	public String get_AllProductAvailability() throws IOException {
		HashMap<String, String> paramsApiCall = new HashMap<String, String>();
		paramsApiCall.clear();
		return caller.callAPI("get_AllProductAvailability", getCurrentTimeStamp(), paramsApiCall);
	}

	/**
	 * Get product formats
	 * 
	 * Retrieves a list of product formats as specified by products
	 *
	 * @return String
	 * @throws IOException
	 */
	public String get_ProductFormats() throws IOException {
		return caller.callAPI("get_ProductFormats", getCurrentTimeStamp());
    }

	/**
	 * Get last sale
	 *
	 * Retrieves the last sale information for the current user
	 *
	 * @return String
	 * @throws IOException
	 */
	public String get_LastSale() throws IOException {
		return caller.callAPI("get_LastSale", getCurrentTimeStamp());
    }