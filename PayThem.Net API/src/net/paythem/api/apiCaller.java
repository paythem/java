package net.paythem.api;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

class apiCaller {
    private String API_VERSION = "2.2.0";

    String SERVER_URI = "";
    boolean DEBUG_OUTPUT = false;
    boolean FAULTY_PROXY = false;
    boolean SERVER_DEBUG = false;
    boolean ENCRYPT_RESPONSE = false;
    int serverConnectTimeout = 15000;
    int serverReadTimeout = 60000;

    JSONObject parameters;
    String PUBLIC_KEY,
            PRIVATE_KEY,
            USERNAME,
            PASSWORD,
            IV,
            SOURCE_IP,
            TIME_ZONE;

    public apiCaller(
            String _domain,
            String _publicKey,
            String _privateKey,
            String _username,
            String _password,
            String _IV,
            String _sourceIP,
            String _timeZone
    ){
        PUBLIC_KEY = _publicKey;
        PRIVATE_KEY = _privateKey;
        USERNAME = _username;
        PASSWORD = _password;
        IV = _IV;
        SOURCE_IP = _sourceIP;
        SERVER_URI = "https://vvs" + _domain + ".paythem.net/API/2824";
        TIME_ZONE = _timeZone;
    }

    public void setConnectTimeout(int _newVal){
    	serverConnectTimeout = _newVal;
    }

    public void setReadTimeout(int _newVal){
    	serverReadTimeout = _newVal;
    }

    public String callAPI(String _function, String _timeStamp) throws IOException {
        return callAPI(_function, _timeStamp, new HashMap<String, String>());
    }

    public String callAPI(String _function, String _timeStamp, HashMap<String, String> _parameters) throws IOException {
        parameters = new JSONObject();
        parameters.put("API_VERSION", API_VERSION);
        parameters.put("SERVER_URI", SERVER_URI);
        parameters.put("SERVER_DEBUG", SERVER_DEBUG);
        parameters.put("FAULTY_PROXY", FAULTY_PROXY);
        parameters.put("USERNAME", USERNAME);
        parameters.put("PASSWORD", PASSWORD);
        parameters.put("PUBLIC_KEY", PUBLIC_KEY);
        parameters.put("SOURCE_IP", SOURCE_IP);
        parameters.put("SERVER_TIMESTAMP", _timeStamp);
        parameters.put("SERVER_TIMEZONE", TIME_ZONE);
        parameters.put("ENCRYPT_RESPONSE", ENCRYPT_RESPONSE);
        parameters.put("FUNCTION", _function);
        parameters.put("PARAMETERS", new JSONObject(_parameters));
        parameters.put("HASH_STUB", util_parameterStringBuilder.randomAlphaNumeric(10));
        String content = parameters.toString();

        String contentPost = util_crypt.encrypt(content, PRIVATE_KEY, IV);
        String hash = util_base64.base64sha256(content, PRIVATE_KEY);

        URL url = new URL(SERVER_URI);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");

        con.setRequestProperty("X-Public-Key", PUBLIC_KEY);
        con.setRequestProperty("X-Hash", hash);
        con.setRequestProperty("X-Sourceip", SOURCE_IP);
        if(FAULTY_PROXY)
            con.setRequestProperty("X-Forwarded-For-Override", SOURCE_IP); // FIX THIS

        Map<String, String> parameters = new HashMap<>();
        parameters.put("PUBLIC_KEY", PUBLIC_KEY);
        parameters.put("CONTENT", contentPost);
        parameters.put("ZAPI", IV);

        con.setDoOutput(true);
        con.setConnectTimeout(serverConnectTimeout);
        con.setReadTimeout(serverReadTimeout);
        DataOutputStream out = new DataOutputStream(con.getOutputStream());
        out.writeBytes(util_parameterStringBuilder.getParamsString(parameters));
        out.flush();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer outContent = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            outContent.append(inputLine);
        }

        in.close();
        out.close();
        con.disconnect();

        return outContent.toString();
    }
}